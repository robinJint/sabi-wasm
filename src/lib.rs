extern crate cfg_if;
extern crate wasm_bindgen;
extern crate sabi;
extern crate combine;
#[macro_use]
extern crate lazy_static;

mod utils;

use std::io;
use std::io::BufWriter;
use cfg_if::cfg_if;
use wasm_bindgen::prelude::*;

use sabi::{vm::{VM, VMError}, parser, compiler};

cfg_if! {
    // When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
    // allocator.
    if #[cfg(feature = "wee_alloc")] {
        extern crate wee_alloc;
        #[global_allocator]
        static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
    }
}

#[wasm_bindgen(module = "../www/index.js")]
extern {
    fn sabi_output(s: &str);
}

#[wasm_bindgen]
extern {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
    fn alert(s: &str);
}

struct JSOutput;

impl io::Write for JSOutput {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        log(&String::from_utf8(buf.into()).unwrap());
        sabi_output(&String::from_utf8(buf.into()).unwrap());
        Ok(buf.len())
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

pub struct JSVM(VM<'static>);

fn add_main(vm: &mut VM) -> Result<(), VMError> {
    vm.parse_ir_block("start", ".data\n0\n.code\nconst 0\ncall main\nhalt")?;
    Ok(())
}

fn setup() -> VM<'static> {
    let mut vm = VM::new(Box::new(BufWriter::new(JSOutput)));
    vm.register_basic();
    vm.register_io();
    vm.register_internal(); 

    vm
}

#[wasm_bindgen]
pub fn run(input: String) {
    let mut vm = setup();
    let toplevel = parser::parse(&input).expect("failed to parse");
    compiler::compile(&mut vm, &toplevel).unwrap();
    add_main(&mut vm).unwrap();

    vm.new_fiber("start").unwrap().run().unwrap();
}