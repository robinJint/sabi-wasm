import * as wasm from "sabi-wasm";

export function sabi_output(s) {
    document.getElementById("output").innerHTML += s
}

document.getElementById("runBtn").addEventListener("click", function(){
    var input = document.getElementById("input").value
    wasm.run(input)
}); 