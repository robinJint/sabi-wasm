/* tslint:disable */
import * as wasm from './sabi_wasm_bg';
import { sabi_output } from '../www/index.js';

let cachedTextDecoder = new TextDecoder('utf-8');

let cachegetUint8Memory = null;
function getUint8Memory() {
    if (cachegetUint8Memory === null || cachegetUint8Memory.buffer !== wasm.memory.buffer) {
        cachegetUint8Memory = new Uint8Array(wasm.memory.buffer);
    }
    return cachegetUint8Memory;
}

function getStringFromWasm(ptr, len) {
    return cachedTextDecoder.decode(getUint8Memory().subarray(ptr, ptr + len));
}

export function __wbg_sabioutput_8bca56813dd51d0e(arg0, arg1) {
    let varg0 = getStringFromWasm(arg0, arg1);
    sabi_output(varg0);
}

const __wbg_log_40fde35340a411df_target = console.log;

export function __wbg_log_40fde35340a411df(arg0, arg1) {
    let varg0 = getStringFromWasm(arg0, arg1);
    __wbg_log_40fde35340a411df_target(varg0);
}

let cachedTextEncoder = new TextEncoder('utf-8');

function passStringToWasm(arg) {

    const buf = cachedTextEncoder.encode(arg);
    const ptr = wasm.__wbindgen_malloc(buf.length);
    getUint8Memory().set(buf, ptr);
    return [ptr, buf.length];
}
/**
* @param {string} arg0
* @returns {void}
*/
export function run(arg0) {
    const [ptr0, len0] = passStringToWasm(arg0);
    return wasm.run(ptr0, len0);
}

export function __wbindgen_throw(ptr, len) {
    throw new Error(getStringFromWasm(ptr, len));
}

